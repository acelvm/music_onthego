@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h3>Classification Details</h3>
					<hr>
					@if($classification)
						<label for="dateCreated">Name : </label>
						<input class="form-control w-50" value="{{ $classification->name }}">
					@endif
			</div>
		</div>
	</div>
@endsection