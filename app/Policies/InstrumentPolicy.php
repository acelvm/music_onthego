<?php

namespace App\Policies;

use App\Instrument;
use App\Classification;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class InstrumentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any products.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the instrument.
     *
     * @param  \App\User  $user
     * @param  \App\Instrument  $instrument
     * @return mixed
     */
    public function view(User $user, Instrument $instrument)
    {
        return $user->role_id === 1;
    }

    /**
     * Determine whether the user can create products.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role_id === 1;
    }

    /**
     * Determine whether the user can update the instrument.
     *
     * @param  \App\User  $user
     * @param  \App\Instrument  $instrument
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->role_id === 1;
    }

    /**
     * Determine whether the user can delete the instrument.
     *
     * @param  \App\User  $user
     * @param  \App\Instrument  $instrument
     * @return mixed
     */
    public function delete(User $user, Instrument $instrument)
    {
        return $user->role_id === 1;
    }

    /**
     * Determine whether the user can restore the instrument.
     *
     * @param  \App\User  $user
     * @param  \App\Instrument  $instrument
     * @return mixed
     */
    public function restore(User $user, Instrument $instrument)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the instrument.
     *
     * @param  \App\User  $user
     * @param  \App\Instrument  $instrument
     * @return mixed
     */
    public function forceDelete(User $user, Instrument $instrument)
    {
        //
    }
}
