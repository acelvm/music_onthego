@extends('layouts.app')
@section('content')

	<div class="container my-5">
		<div class="row">
			@include('includes.error-status')
			{{-- start of cards --}}
			<div class="col-12 col-sm-6 col-md-4 my-3">
				<img src="/public/{{ $instrument->image}}" alt="..." class="card-img-top">
			</div>

			<div class="col-12 col-sm-6 col-md-8 my-3">
				<h5>{{ $instrument->name }}</h5>
				<p class="mb-1">&#8369; {{ number_format($instrument->price,2) }} / day
				</p>
				<p class="card-text">{{ $instrument->classification->name }}</p>
				<p class="description">{{ $instrument->description }}</p>

				@cannot('isAdmin')
					<form action="{{ route('carts.update',['cart' => $instrument->id]) }}" method="post">
						@csrf
						@method('PUT')
						<div class="form-group row">
							<div class="col-12 col-sm-6 col-md-4">
								<input type="number" name="quantity" id="quantity" class="form-control mb-2" min="1" placeholder="No. of Days">
								<button class="btn btn-outline-primary w-100 mb-2">Rent Instrument</button>
							</div>
						</div>
					</form>
				@endcannot

				@can('isAdmin')
					<div class="row">
						<div class="col-12 col-sm-6 col-md-4">
							<a href="{{ route('instruments.edit',['instrument' => $instrument->id])}}" class="btn btn-outline-warning w-100 mb-2">Edit Instrument</a>
						</div>
					</div>
					<form action="{{ route('instruments.destroy',['instrument' => $instrument->id])}}" method="post">
						<div class="form-group row">
							<div class="col-12 col-sm-6 col-md-4">
								@csrf
								@method('DELETE')
								<button type="submit" class="btn btn-outline-danger w-100 mb-2">Delete Instrument</button>
							</div>
						</div>
					</form>
				@endcan
			</div>
			{{-- end of cards --}}
		</div>
	</div>
@endsection