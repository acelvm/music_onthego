<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Instrument;
use App\Classification;
use App\Policies\InstrumentPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = 
    [
        Instrument::class => InstrumentPolicy::class,
        Classification::class => InstrumentPolicy::class,
        // 'App\Transaction' => 'App\Policies\TransactionPolicy'
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAdmin', function ($user)
        {
            return $user->role_id === 1;
        });

        Gate::define('isLogged', function($user)
        {
            return $user !== null;
        });
    }
}
