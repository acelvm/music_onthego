@extends('layouts.app')
@section('content')
	<script src="https://www.paypal.com/sdk/js?client-id=AYljKuxlXktf5tmADA6ZFft9YlEMza13XLwpvN0iOT7I91vmxzSzr2GHpHgtNv_ZdNXR26h7rPVVqlgK"></script>
	<div class="container">
		<div class="row">
			<div class="col-10">
				<h3>My Cart</h3>
			</div>

			@include('includes.error-status')
			
			@if(Session::has('cart'))
				<div class="col-2 ml-auto">
					<form action="{{ route('carts.empty')}}" method="post">
						@csrf
						@method('DELETE')
						<button class="btn btn-outline-danger w-75">Clear Cart</button>
					</form>
				</div>
				<div class="col-12">
					@if(Session::has('status'))
						<div class="col-12">
							<div class="alert alert-danger">
								{{ Session::get('status') }}
							</div>
						</div>
					@endif
					<!-- start of table -->
					<div class="table-responsive">
						<table class="table table-striped table-hover text-center">
							<thead>
								<th scope="col">Name</th>
								<th scope="col">Price per Unit</th>
								<th scope="col">No. of Days</th>
								<th scope="col">Subtotal</th>
								<th scope="col">Action</th>
							</thead>
							<tbody>
								@foreach($instruments as $instrument)
								<!-- start of row -->
								<tr>
									<th scope="row">{{ $instrument->name }}</th>
									<td>&#8369; <span>{{ number_format($instrument->price,2) }}</span></td>
									<td>
										<div class="ad-tocart-filed mb-1">
											<form action="{{ route('carts.update', ['cart' => $instrument->id]) }}" method="post">
												@csrf
												@method('PUT')
												<input type="number" name="quantity" id="quantity" class="form-control w-25 mx-auto" min="1" value="{{ $instrument->quantity }}">
												<button class="btn btn-outline-secondary w-25" type="submit">Edit</button>
											</form>
										</div>
									</td>
									<td>&#8369; <span>{{ number_format($instrument->subtotal,2) }}</span></td>
									<td>
										<form action="{{ route('carts.destroy', ['cart' => $instrument->id]) }}" method="post">
											@csrf
											@method('DELETE')
											<button class="btn btn-outline-danger w-75">Remove from cart</button>
										</form>
									</td>
								</tr>
								<!-- end of row -->
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td colspan="3" class="text-right"><strong>Total</strong></td>
									<td>&#8369; <span id="total">{{ number_format($total,2) }}</span></td>
									<td>
										@can('isLogged')
											<form action="{{ route('transactions.store') }}" method="post">
												@csrf
												<button class="btn btn-outline-primary w-75 mb-2">Check-out</button>
											</form>
											<div id="paypal-btn" class="w-75 mx-auto"></div>

										@endcan
										@cannot('isLogged')
											<a href="{{ route('login') }}" class="btn btn-outline-primary w-75">Log-in to continue</a>
										@endcannot
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
					<!-- end of table -->
				</div>
			@else
			@if(Session::has('status'))
				<div class="col-12">
					<div class="alert alert-danger">
						{{ Session::get('status') }}
					</div>
				</div>
			@endif
			@endif
		</div>
	</div>
	<script>
		paypal.Buttons(
		{
			createOrder: function(data, actions) 
			{
				// This function sets up the details of the transaction, including the amount and line item details.
				return actions.order.create(
				{
					purchase_units: 
					[{
						amount: 
						{
							value: {{ $total ?? '' }}
						}
					}]
				});
			},
	
			onApprove: function(data, actions) 
			{
				// This function captures the funds from the transaction.
				return actions.order.capture().then(function(details) 
				{
					// This function shows a transaction success message to your buyer.
					alert('Transaction completed by ' + data.orderID);

					let csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
					console.log(csrfToken);
					let data = { transactionCode : data.orderID};

					fetch('{{ route('transactions.paypal') }}', 
					{
						method : 'post',
						body : JSON.stringify(data),
						headers : { 'X-CSRF-TOKEN' : csrfToken }
					})
					.then( response => response.json())
					.then( res => console.log(res));
				});
			}
		}).render('#paypal-btn');
	</script>
@endsection