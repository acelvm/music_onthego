@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h3>Edit Instrument Form</h3>
				<hr>
				@if(Session::has('status'))
					<div class="alert alert-success">
						{{ Session::get('status') }}
					</div>
				@endif
				<form action="{{ route('instruments.update',['instrument' => $instrument->id])}}" method="post" enctype="multipart/form-data">
					@csrf
					@method('PUT')

					<input type="text" name="name" id="name" class="form-control mb-2" placeholder="Instrument Name" value="{{ $instrument->name }}">
					@if($errors->has('name'))
					<div class="alert alert-danger">
						{{ $errors->first('name')}}
					</div>
					@endif

					<input type="text" name="price" id="price" class="form-control mb-2" placeholder="Price" value="{{ $instrument->price }}">
					@if($errors->has('price'))
					<div class="alert alert-danger">
						{{ $errors->first('price')}}
					</div>
					@endif

					<select name="classification-id" id="classification-id" class="custom-select mb-2">
						@foreach($classifications as $classification)
							<option value="{{ $classification->id }}" 
								{{ $instrument->classification_id == $classification->id ? "selected" : ""}}>
								{{ $classification->name }}
							</option>
						@endforeach
					</select>
						@if($errors->has('classification-id'))
							<div class="alert alert-danger">
								{{ $errors->first('classification-id')}}
							</div>
						@endif

					<input type="file" name="image" id="image" class="form-control-file mb-2">
					@if($errors->has('image'))
					<div class="alert alert-danger">
						{{ $errors->first('image')}}
					</div>
					@endif

					<textarea name="description" id="description" class="form-control mb-3" cols="10" rows="5" placeholder="Product Description">{{ $instrument->description }}</textarea>
					@if($errors->has('description'))
					<div class="alert alert-danger">
						{{ $errors->first('description') }}
					</div>
					@endif

					<button type="submit" class="btn btn-primary">Save</button>
				</form>
			</div>
			<div class="col-12 col-sm-6 col-md-4 my-3">
				<div class="card">
					<div class="card-body">
						<img src="/public/{{ $instrument->image}}" alt="..." class="card-img-top">
						<h5>{{ $instrument->name }}</h5>
						<p class="mb-1">&#8369; {{ number_format($instrument->price,2) }}
						</p>
						<p class="card-text">{{ $instrument->classification->name }}</p>
						<p class="description">{{ $instrument->description }}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection