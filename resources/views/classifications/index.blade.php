@extends('layouts.app')
@section('content')

	<div class="container my-5">
		<div class="row">
			<div class="col-12 col-md-12">
				<table class="table text-center">
						<thead>
							<tr>
								<th scope="col">Classification</th>
								<th scope="col">Actions</th>
							</tr>
						</thead>
				@foreach($classifications as $classification)
						<tbody>
							<tr>
								<td>{{ $classification->name }}</td>
								<td>
									<a href="{{ route('classifications.show',['classification' => $classification->id]) }}">Details</a> |
									<a href="{{ route('classifications.edit',['classification' => $classification->id])}}">Update</a>
									<form action="{{ route('classifications.destroy',['classification' => $classification->id])}}" method="post">
										@csrf
										@method('DELETE')
										<button type="submit">Delete</button>
									</form>
								</td>
							</tr>
						</tbody>
				@endforeach
				</table>
			</div>
		</div>
	</div>
@endsection
