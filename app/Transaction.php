<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function status()
	{
		return $this->belongsTo('App\Status');
	}

	public function payment_mode()
	{
		return $this->belongsTo('App\PaymentMode');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function instruments()
	{
		return $this->belongsToMany('App\Instrument','instrument_transaction')
			->withPivot('quantity','subtotal','price');
	}
}
