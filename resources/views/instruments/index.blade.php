@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 mb-3">
				<div class="sticky-top">
					<form action="">
						@csrf
						<div class="row">
							<div class="col-12 col-md-4">
								<select name="classification" id="classification" class="form-control w-75">
									<option value="">All</option>
									@foreach($classifications as $classification)
									<option value="{{ $classification->id }}">{{ $classification->name }}</option>
									@endforeach
								</select>

							</div>
							<div class="col-12">
								<button class="btn btn-outline-primary">Filter</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			@if(Session::has('status'))
				<div class="alert alert-danger col-12">
					{{ Session::get('status') }}
				</div>
			@endif
			
			@include('includes.error-status')

			@foreach($instruments as $instrument)
			{{-- start of cards --}}
			<div class="col-12 col-md-4 col-lg-3">
				@include('includes.instrument-card')
			</div>
			{{-- end of cards --}}
			@endforeach
		</div>
	</div>
@endsection