<?php

use Illuminate\Database\Seeder;

class InstrumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('instruments')->insert(
        [
        	'name' => 'Guitar',
        	'price' => '867.00',
        	'description' => 'guitar wood',
        	'classification_id' => 1,
        	'image' => 'public/3usiAoboQ6cyCfsTFxVQAn5NjK3SzgMvFGuinJoB.jpeg'
        ]);
    }
}
