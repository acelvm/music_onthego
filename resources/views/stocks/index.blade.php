@extends('layouts.app')
@section('content')
  <div class="container">
    @if(Session::has('status'))
      <div class="alert alert-success col-12">
        {{Session::get('status')}}
      </div>
    @endif
    
    <div class="row">
      <div class="col-12 col-md-3">
        @include('stocks.create')
      </div>
      
      <div class="col-12 col-md-9">
        <table class="table table-striped table-light">
          <thead>
            <tr>
                <th scope="col">Instrument</th>
                <th scope="col">Serial number</th>
                <th scope="col">Current status</th>
                <th scope="col"></th>
                <th scope="colspan-2">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($stocks as $stock)
            <tr>
              <td>{{ $stock->instrument->name }}</td>
              <td>{{ $stock->serial }}</td> 
              <td>{{ $stock->stock_status }}</td>
              <form action="{{ route('stocks.update',['stock' => $stock->id]) }}" method="POST">
                @csrf
                @method('PUT')
                <td>
                  <select name="stock_status" id="stock_status" class="custom-select mb-1">
                    <option disabled selected="">Select status</option>
                    <option value="Available">Available</option>
                    <option value="Rented">Rented</option>
                  </select>
                </td>  
                <td>
                  <button type="submit" class="btn btn-outline-warning text-dark">
                    Change status
                  </button>
                </td>
              </form>

              <form action="{{ route('stocks.destroy',['stock' => $stock->id] )}}" method="POST">
                @csrf
                @method('DELETE')
                <td>
                  <button class="btn btn-danger w-100 mb-1">Delete</button>
                </td> 
              </form>    
            </tr>
            @endforeach
          </tbody>
      </table>
      </div>
    </div>
  </div>
@endsection