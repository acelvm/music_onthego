@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<h3>Rental Requests</h3>
			<hr>
		</div>
		<div class="col-12">
			{{-- start of accordion --}}
			<div class="accordion" id="accordionExample">
				@foreach($transactions as $transaction)
					<div class="card">
						<div class="card-header" id="headingOne">
							<h2 class="mb-0">
								<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#transaction-{{ $transaction->id}}" aria-expanded="true" aria-controls="collapseOne">
									Request # {{ $transaction->transaction_code}}
									<span class="badge badge-warning float-right">{{ $transaction->status->name }}</span>
								</button>
							</h2>
						</div>

						<div id="transaction-{{ $transaction->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
							
							<div class="card-body">
								<div class="col-12">
									{{-- start of table --}}
									<div class="table-responsive">
										@include('transactions.includes.transaction-table')
										<a href="{{ route('transactions.show', ['transaction' => $transaction->id]) }}" class="btn btn-outline-primary">View Details</a>
									</div>
									{{-- end of table --}}
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
			{{-- end of accordion --}}
		</div>
	</div>
</div>
@endsection