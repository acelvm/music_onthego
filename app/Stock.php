<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $guarded = [];

    public function instrument()
    {
    	return $this->belongsTo(Instrument::class);
    }
}
