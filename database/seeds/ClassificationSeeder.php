<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classifications')->insert(
        [
        	'name' => 'Stringed'
        ]);

        DB::table('classifications')->insert(
        [
        	'name' => 'Percussion'
        ]);

        DB::table('classifications')->insert(
        [
        	'name' => 'Keyboard'
        ]);
    }
}
