<div class="container">
  <div class="row">
    <div class="col-12 col-md-8 offset-md-2">
      <form action="{{ route('stocks.update',['stock' => $stock->id]) }}" method="POST">
        @csrf
        @method('PUT')

        <div>
          @if($errors->has('instrument_id'))
            <div class="alert alert-danger">
              {{ $errors->first('instrument-id')}}
            </div>
          @endif
          {{ $stock->instrument->title }}
          <input type="text" name="serial" value="{{ old('serial') ?? $stock->serial }}">
        </div>
        
        <div>
          <select name="stock_status" id="stock_status" class="custom-select mb-1">
            <option value="Available">Available</option>
            <option value="Rented">Rented</option>
          </select>
        </div>
          
        <div>
          <button type="submit" class="btn btn-success">Add stock</button>
        </div>
        
      </form>
  </div>
</div>