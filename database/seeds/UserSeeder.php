<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
        [
        	'name' => 'Abbie',
        	'email' => 'abbie@email.com',
        	'password' => Hash::make('abbie123'),
            'role_id' => 2
        ]);

        DB::table('users')->insert(
        [
            'name' => 'User',
            'email' => 'user@email.com',
            'password' => Hash::make('user123'),
            'role_id' => 2
        ]);

        DB::table('users')->insert(
        [
            'name' => 'Admin',
            'email' => 'admin@email.com',
            'password' => Hash::make('test123'),
            'role_id' => 1
        ]);
    }
}
