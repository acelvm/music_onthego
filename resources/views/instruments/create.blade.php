@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mc-auto">
				<h3>Add Instrument Form</h3>
				<hr>
				<form action="{{ route('instruments.store')}}" method="post" enctype="multipart/form-data">
					@csrf

					<input type="text" name="name" id="name" class="form-control mb-2" placeholder="Instrument Name" value="{{ old('name') }}">
						@if($errors->has('name'))
							<div class="alert alert-danger">
								{{ $errors->first('name')}}
							</div>
						@endif

					<input type="text" name="price" id="price" class="form-control mb-2" placeholder="Price" value="{{ old('price') }}">
						@if($errors->has('price'))
							<div class="alert alert-danger">
								{{ $errors->first('price')}}
							</div>
						@endif

					<select name="classification-id" id="classification-id" class="custom-select mb-2">
						@foreach($classifications as $classification)
							<option value="{{ $classification->id }}" 
								{{ old('classification-id') == $classification->id ? "selected" : ""}}>
								{{ $classification->name }}
							</option>
						@endforeach
					</select>
						@if($errors->has('classification-id'))
							<div class="alert alert-danger">
								{{ $errors->first('classification-id')}}
							</div>
						@endif
					
					<input type="file" name="image" id="image" class="form-control-file mb-2">
						@if($errors->has('image'))
							<div class="alert alert-danger">
								{{ $errors->first('image')}}
							</div>
						@endif

					<textarea name="description" id="description" class="form-control mb-3" cols="10" rows="5" placeholder="Instrument Description">{{ old('description') }}</textarea>
						@if($errors->has('description'))
							<div class="alert alert-danger">
								{{ $errors->first('description') }}
							</div>
						@endif

					<button type="submit" class="btn btn-primary">Add Instruments</button>
				</form>
			</div>
		</div>
	</div>
@endsection