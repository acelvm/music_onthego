<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instrument extends Model
{
    public function classification()
    {
    	return $this->belongsTo('App\Classification');
    }
}
