@extends('layouts.app')
@section('content')

	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3>Rental Requests</h3>
				<hr>
			</div>
			
			<div class="col-12">
				{{-- start of table --}}
				<div class="table-responsive">
					{{-- start of transaction table --}}
						@include('transactions.includes.transaction-table')
					{{-- end of transaction table --}}
				
					{{-- start of product_transaction table --}}
					<table class="table table-striped table-hover">
						<thead>
							<th scope="row">Instrument Name</th>
							<th scope="row">Price</th>
							<th scope="row">No. of Days</th>
							<th scope="row">Subtotal</th>
						</thead>
						<tbody>
							{{-- start of product_transaction details --}}
							@foreach($transaction->instruments as $transaction_instrument)
								<tr>
									<td>
										{{ $transaction_instrument->name }}
									</td>
									<td>
										&#8369; {{ number_format($transaction_instrument->pivot->price,2) }}
									</td>
									<td>
										{{ $transaction_instrument->pivot->quantity }}
									</td>
									<td>
										&#8369; {{ number_format($transaction_instrument->pivot->subtotal,2) }}
									</td>
								</tr>
							@endforeach
							
							{{-- end of product_transaction details --}}
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3" class="text-right"><strong>Total</strong></td>
								<td>&#8369; {{ number_format($transaction->total,2) }}</td>
							</tr>
						</tfoot>
					</table>
					{{-- end of product_transaction table --}}
				</div>
				{{-- end of table --}}

			</div>
			
		</div>
	</div>

@endsection