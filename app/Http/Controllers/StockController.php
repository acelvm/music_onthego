<?php

namespace App\Http\Controllers;

use App\Stock;
use App\Instrument;
use Str;
use Illuminate\Http\Request;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Stock $stock)
    {
        // $this->authorize('view' ,$stock);
        $stocks = Stock::all();

        return view('stocks.index')
            ->with('stocks', $stocks)
            ->with('instruments', Instrument::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Stock $stock)
    {
        $this->authorize('create' ,$stock);

        $instruments = Instrument::all();
        return view('stocks.create')
            ->with('instruments', $movies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Stock $stock)
    {
        $request->validate([
            'instrument_id' => 'required',
            'stock_status' => 'required'
        ]);

        $stock = new Stock;
        $stock->instrument_id = $request->input('instrument_id');
        $stock->serial = Str::random(10);
        $stock->stock_status = $request->input('stock_status');

        $stock->save();

        $instrument_stocks = Instrument::all();

        foreach ($instrument_stocks as $instruments) 
        {
            $stocks = Stock::all()
                ->whereIn('instrument_id', $instruments->id)
                ->whereIn('stock_status', 'Available');
            $instruments->stock = count($stocks);
            $instruments->save();
        }

        return redirect( route('stocks.index') );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit(Stock $stock)
    {
        return view('stocks.edit')
            ->with('stock', $stock)
            ->with('instruments', Instrument::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stock $stock)
    {
        $request->validate([
            'stock_status' => 'required'
        ]);

        $stock->stock_status = $request->input('stock_status');
        $stock->save();
        $instrument_stocks = Instrument::all();

        foreach ($instrument_stocks as $instruments) 
        {
            $stocks = Stock::all()
                ->whereIn('instrument_id', $instruments->id)
                ->whereIn('stock_status', 'Available');
            $instruments->stock = count($stocks);
            $instruments->save();
        }

        return redirect( route('stocks.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        $stock->delete();

        $instrument_stocks = Instrument::all();

        foreach ($instrument_stocks as $instruments) 
        {
            $stocks = Stock::all()
                ->whereIn('instrument_id', $instruments->id)
                ->whereIn('stock_status', 'Available');
            $instruments->stock = count($stocks);
            $instruments->save();
        }
        return redirect( route('stocks.index'))
            ->with('status', 'Stock deleted!');        
    }
}
