<table class="table table-striped">
	<tbody>
		<tr>
			<td>Customer's Name:</td>
			<td>{{ $transaction->user->email }}</td>
		</tr>
		<tr>
			<td>Transaction Code:</td>
			<td>{{ $transaction->transaction_code }}</td>
		</tr>
		<tr>
			<td>Payment Mode:</td>
			<td>{{ $transaction->payment_mode->name }}</td>
		</tr>
		<tr>
			<td>Request Created:</td>
			<td>{{ $transaction->created_at->format('F d, Y') }}</td>
		</tr>
		<tr>
			<td>Status:</td>
			<td>
				@can('isAdmin')
					<form action="{{ route('transactions.update', ['transaction'=>$transaction->id]) }}" method="post">
						@csrf
						@method('PUT')

						<select name="status" id="status" class="custom-select w-25">
							@foreach($statuses as $status)
								<option value="{{ $status->id }}">
									{{ $status->name }}
								</option>
							@endforeach
						</select>
						<button class="btn btn-outline-primary">Edit Status</button>
					</form>
				@endcan
					
				@cannot('isAdmin')

				@endcannot
			</td>
		</tr>
	</tbody>
</table>