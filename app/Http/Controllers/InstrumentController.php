<?php

namespace App\Http\Controllers;

use App\Instrument;
use App\Classification;
use Illuminate\Http\Request;

class InstrumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->query('classification');
        if ($filter) {
            $instruments = Instrument::all()->whereIn('classification_id', $filter);
        } else 
            {
                $instruments = Instrument::all();
            }
        return view('instruments.index')
            ->with('instruments',$instruments)
            ->with('classifications',Classification::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Instrument $instrument)
    {
        $this->authorize('create', $instrument);
        $classifications = Classification::all();
        return view('instruments.create')->with('classifications',$classifications);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Instrument $instrument)
    {
        $this->authorize('create', $instrument);
        $request->validate(
        [
            'name' => 'required|string',
            'price' => 'required|numeric',
            'classification-id' => 'required',
            'description' => 'required|string',
            'image' => 'required|image|max:10000'
        ]);

        $instrument = new Instrument;
        $instrument->name = $request->input('name');
        $instrument->price = $request->input('price');
        $instrument->classification_id = $request->input('classification-id');
        $instrument->description = $request->input('description');

        $instrument->image = $request->image->store('public');
        $instrument->save();
        return redirect( route('instruments.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Instrument  $instrument
     * @return \Illuminate\Http\Response
     */
    public function show(Instrument $instrument)
    {
        return view('instruments.show')->with('instrument',$instrument);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Instrument  $instrument
     * @return \Illuminate\Http\Response
     */
    public function edit(Instrument $instrument)
    {
        $this->authorize('update', $instrument);
        return view('instruments.edit')
           ->with('instrument',$instrument)
           ->with('classifications',Classification::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Instrument  $instrument
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Instrument $instrument)
    {
        $this->authorize('update', $instrument); 
        $request->validate([
            'name' => 'required|string',
            'price' => 'required|numeric',
            'classification-id' => 'required',
            'description' => 'required|string',
            'image' => 'image|max:10000'
        ]);

        $instrument->name = $request->input('name');
        $instrument->price = $request->input('price');
        $instrument->classification_id = $request->input('classification-id');
        $instrument->description = $request->input('description');

        if ($request->hasFile('image')) 
        {
            $instrument->image = $request->image->store('public');
        }
        $instrument->save();
        $request->session()->flash('status','Instrument updated successfully!');
        return redirect( route('instruments.edit',['instrument' => $instrument->id]) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Instrument  $instrument
     * @return \Illuminate\Http\Response
     */
    public function destroy(Instrument $instrument)
    {
        $instrument->delete();
        return redirect( route('instruments.index'))->with('status','Instrument is deleted!');
    }
}
