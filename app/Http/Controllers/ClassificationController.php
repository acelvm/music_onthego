<?php

namespace App\Http\Controllers;

use App\Classification;
use Illuminate\Http\Request;

class ClassificationController extends Controller
{
	public function index(Classification $classification)
    {
    	// $this->authorize('view', $classification);
        $classifications = Classification::all();
    	return view('classifications.index')
    		->with('classifications',$classifications);
    }

    public function show(Classification $classification)
    {
    	return view('classifications.show')->with('classification',$classification);
    }

    public function create(Classification $classification)
    {
    	$this->authorize('create', $classification);
        return view('classifications.create');
    }

    public function store(Request $request, Classification $classification)
    {
    	$this->authorize('create', $classification);
        $request->validate([
    		'name' => 'string|required|max:50|unique:classifications,name'
    	]);

    	$classification = new Classification;
    	$name = $request->input('name');
    	$classification->name = $name;
    	$classification->save();
    	return redirect( route('classifications.index'));
    }

    public function edit(Classification $classification)
    {
 
    	$this->authorize('update', $classification);
        return view('classifications.edit')->with('classification',$classification);
    }

    public function update(Classification $classification, Request $request)
    {
    	$this->authorize('update', $classification);
        $request->validate([
    		'name' => 'string|required|max:50|unique:classifications,name'
    	]);
    	
    	$classification->name = $request->input('name');
    	$classification->save();
    	return redirect( route('classifications.show',['classification' => $classification->id]));
    }

    public function destroy(Classification $classification)
    {

    	$classification->delete();
    	return redirect( route('classifications.index'));
    }
}
