<form action="{{ route('stocks.store') }}" method="POST" enctype="multipart/form-data">
	@csrf
	<div>
		@if($errors->has('instrument_id'))
		<div class="alert alert-danger">
			{{ $errors->first('instrument-id')}}No instrument selected
		</div>
		@endif

		<select name="instrument_id" id="instrument_id" class="custom-select mb-1">
			<option selected disabled>Select Instrument</option>
				@foreach($instruments as $instrument)
					<option value="{{ $instrument->id }}"
						{{ old('instrument_id') === $instrument->id ? "selected" : "" }}>
						{{ $instrument->name }}
					</option>
				@endforeach
			</select>
		</div>
		<div class="invisible">
			<select name="stock_status" id="stock_status" class="custom-select mb-1">
				<option value="Available">Available</option>
				<option value="Rented">Rented</option>
			</select>
		</div>

		<div>
			<button type="submit" class="btn btn-warning">Add stock</button>
		</div>

	</form>

	<div class="mt-3">
		<table class="table table-striped table-light">
			<thead>
				<tr>
					<th scope="col">Instrument Name</th>
					<th scope="col">Stocks</th>
				</tr>
			</thead>
			<tbody>

				@foreach($instruments as $instrument)
				<tr>
					<td>{{ $instrument->name }}</td>
					<td>{{ $instrument->stock }}</td>        
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>