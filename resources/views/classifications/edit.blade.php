@extends('layouts.app')
@section('content')
	@include('includes.error-status')
	
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<form action="{{ route('classifications.update',['classification' => $classification->id])}}" method="post">
					@method('PUT')
					@csrf

					<label for="name">Classification Name:</label>
					<input type="text" name="name" value="{{ $classification->name }}">
					<button type="submit">Update</button>
				</form>
			</div>
		</div>
	</div>
	
@endsection