<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () 
{
    return view('welcome');
});


Route::post('/transactions/paypal', 'TransactionController@create_paypal_payment')->name('transactions.paypal');

/*Start of Classification*/
Route::get('/classifications/create', 'ClassificationController@create')->name('classifications.create');
Route::get('/classifications', 'ClassificationController@index')->name('classifications.index');
Route::get('/classifications/{classification}', 'ClassificationController@show')->name('classifications.show');
Route::get('/classifications/{classification}/edit', 'ClassificationController@edit')->name('classifications.edit');
Route::post('/classifications', 'ClassificationController@store')->name('classifications.store');
Route::put('/classifications/{classification}','ClassificationController@update')->name('classifications.update');
Route::delete('/classifications/{classification}', 'ClassificationController@destroy')->name('classifications.destroy');
/*End of Classification*/

/*Start of Instrument*/
Route::get('/instruments/create', 'InstrumentController@create')->name('instruments.create');
Route::get('/instruments', 'InstrumentController@index')->name('instruments.index');
Route::get('/instruments/{instrument}', 'InstrumentController@show')->name('instruments.show');
Route::get('/instruments/{instrument}/edit', 'InstrumentController@edit')->name('instruments.edit');
Route::post('/instruments', 'InstrumentController@store')->name('instruments.store');
Route::put('/instruments/{instrument}','InstrumentController@update')->name('instruments.update');
Route::delete('/instruments/{instrument}', 'InstrumentController@destroy')->name('instruments.destroy');
/*End of Instrument*/

/*Start of Stocks*/
Route::get('/stocks/create', 'StockController@create')->name('stocks.create');
Route::get('/stocks', 'StockController@index')->name('stocks.index');
Route::get('/stocks/{stock}', 'StockController@show')->name('stocks.show');
Route::get('/stocks/{stock}/edit', 'StockController@edit')->name('stocks.edit');
Route::post('/stocks', 'StockController@store')->name('stocks.store');
Route::put('/stocks/{stock}','StockController@update')->name('stocks.update');
Route::delete('/stocks/{stock}', 'StockController@destroy')->name('stocks.destroy');
/*End of Stocks*/

/*Start of Cart*/
Route::get('/carts/create', 'CartController@create')->name('carts.create');
Route::get('/carts', 'CartController@index')->name('carts.index');
Route::get('/carts/{cart}', 'CartController@show')->name('carts.show');
Route::get('/carts/{cart}/edit', 'CartController@edit')->name('carts.edit');
Route::post('/carts', 'CartController@store')->name('carts.store');
Route::put('/carts/{cart}','CartController@update')->name('carts.update');
Route::delete('/carts/empty','CartController@empty')->name('carts.empty');
Route::delete('/carts/{cart}', 'CartController@destroy')->name('carts.destroy');
/*End of Cart*/

/*Start of Transactions*/
Route::get('/transactions/create', 'TransactionController@create')->name('transactions.create');
Route::get('/transactions', 'TransactionController@index')->name('transactions.index');
Route::get('/transactions/{transaction}', 'TransactionController@show')->name('transactions.show');
Route::get('/transactions/{transaction}/edit', 'TransactionController@edit')->name('transactions.edit');
Route::post('/transactions', 'TransactionController@store')->name('transactions.store');
Route::put('/transactions/{transaction}','TransactionController@update')->name('transactions.update');
Route::delete('/transactions/{transaction}', 'TransactionController@destroy')->name('transactions.destroy');
/*End of Transactions*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
