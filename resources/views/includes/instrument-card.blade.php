<div class="card mb-3">
	<img src="/public/{{ $instrument->image}}" alt="..." class="card-img-top">
	<div class="card-body">
		<h5 class="card-title">{{ $instrument->name }}</h5>
		<p class="card-text">&#8369; {{ number_format($instrument->price,2) }} / hour</p>
		<p class="card-text">{{ $instrument->classification->name }}</p>
		<p class="card-text">{{ $instrument->description }}</p>
		<p class="card-text">Available Stocks: {{ $instrument->stock }}</p>
	</div>
	<div class="card-footer">
		@cannot('isAdmin')
			<form action="{{ route('carts.update',['cart' => $instrument->id]) }}" method="post">
				@csrf
				@method('PUT')

				<input type="number" name="quantity" id="quantity" class="form-control mb-2" placeholder="No. of Days" min="1">

				<button class="btn btn-outline-primary w-100 mb-2" {{ $instrument->stock == 0 ? 'disabled' : '' }}>Rent Instrument</button>
			</form>
		@endcannot
		<a href="{{ route('instruments.show',['instrument' => $instrument->id])}}" class="btn btn-outline-success w-100 mb-2">View Instrument</a>
		
		@can('isAdmin')
			<a href="{{ route('instruments.edit',['instrument' => $instrument->id])}}" class="btn btn-outline-warning w-100 mb-2">Edit Instrument</a>
			
			<form action="{{ route('instruments.destroy',['instrument' => $instrument->id])}}" method="post">
				@csrf
				@method('DELETE')
				<button type="submit" class="btn btn-outline-danger w-100 mb-2">Delete Instrument</button>
			</form>
		@endcan
	</div>
</div>