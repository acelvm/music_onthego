<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstrumentTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrument_transaction', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id')->nullable();
            $table->foreign('transaction_id')
                ->references('id')->on('transactions')
                ->onDelete('set null')
                ->onUpdate('set null');

            $table->unsignedBigInteger('instrument_id')->nullable();
            $table->foreign('instrument_id')
                ->references('id')->on('instruments')
                ->onDelete('set null')
                ->onUpdate('set null');

            $table->integer('quantity');
            $table->float('subtotal');
            $table->float('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instrument_transaction');
    }
}
