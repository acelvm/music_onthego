@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h3>Create Classification</h3>
					<hr>
				<form action="/classifications" method="post">
					@csrf

					@if($errors->any())
					<div>
						@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</div>
					@endif
					
					<div class="form-group row">
						<label for="name" class="col-md-4 col-form-label text-md-right">Classification Name:</label>

						<div class="col-md-6">
							<input type="text" name="name" class="form-control w-100">
						</div>
					</div>

					<div class="form-group col-md-12 text-center">
						<button type="submit" class="btn btn-primary">Add Classification</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection